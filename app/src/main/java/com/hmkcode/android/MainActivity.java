package com.hmkcode.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.hmkcode.android.vo.GPS;

public class MainActivity extends appCompatActivity implements OnClickListener,LocationListener {

	TextView tvIsConnected, locStat;
	TextView tvLatitude,tvLongitude,tvAltitude,tvAccuracy;
	Button btnPost;
	Double currentLat = null, curretLong = null, currentAlt = null;
	Float currentAcc = null;
	LocationManager locationManager;
	String PROVIDER = LocationManager.GPS_PROVIDER;

	GPS gps;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initilizeView();
		// check if you are connected or not
		if(isConnected()){
			tvIsConnected.setBackgroundColor(0xFF00CC00);
			tvIsConnected.setText("You are conncted");
        }
		else{
			tvIsConnected.setText("You are NOT conncted");
		}
		
		// add click listener to Button "POST"
		btnPost.setOnClickListener(this);

		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		final Location location =  locationManager.getLastKnownLocation(PROVIDER);
		showCurrentLocation(location);
	}

	public void initilizeView(){
		tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
		locStat = (TextView)findViewById(R.id.locStat);
		tvLatitude = (TextView) findViewById(R.id.tvLatitude);
		tvLongitude = (TextView) findViewById(R.id.tvLongitude);
		tvAltitude = (TextView) findViewById(R.id.tvAltitude);
		tvAccuracy = (TextView) findViewById(R.id.tvAccuracy);
		btnPost = (Button) findViewById(R.id.btnPost);
	}

	public static String POST(String url, GPS gps){
		InputStream inputStream = null;
		String result = "";
		try {
			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();
			
			// 2. make POST request to the given URL
		    HttpPost httpPost = new HttpPost(url);
		    
		    String json = "";

		    // 3. build jsonObject
		    JSONObject jsonObject = new JSONObject();
		    jsonObject.accumulate("latitude", gps.getLatitude());
		    jsonObject.accumulate("longitude", gps.getLongitude());
		    jsonObject.accumulate("altitude", gps.getAltitude());
			jsonObject.accumulate("accuracy", gps.getAccuracy());
		    
		    // 4. convert JSONObject to JSON to String
		    json = jsonObject.toString();

		    
		    // ** Alternative way to convert GPS object to JSON string usin Jackson Lib
		    // ObjectMapper mapper = new ObjectMapper();
		    // json = mapper.writeValueAsString(GPS);
		    
		    // 5. set json to StringEntity
		    StringEntity se = new StringEntity(json);
		    
		    // 6. set httpPost Entity
		    httpPost.setEntity(se);
		    
		    // 7. Set some headers to inform server about the type of the content   
		    httpPost.setHeader("Accept", "application/json");
		    httpPost.setHeader("Content-type", "application/json");
		    
			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);
			
			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// 10. convert inputstream to string
			if(inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";
		
		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}
		
		// 11. return result
		return result;
	}
	
	 @Override
		public void onClick(View view) {
		
			switch(view.getId()){
				case R.id.btnPost:
//					if(!validate())
//						Toast.makeText(getBaseContext(), "Enter some data!", Toast.LENGTH_LONG).show();
					// call AsynTask to perform network operation on separate thread
					new HttpAsyncTask().execute("http://192.168.0.32:8000/api/gps");
				break;
			}
			
		}
	
    public boolean isConnected(){
    	ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
    	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
    	    if (networkInfo != null && networkInfo.isConnected()) 
    	    	return true;
    	    else
    	    	return false;	
    }

//	public void stopTracking(Context context) {
//			if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//				return;
//			}
//			locationManager.removeUpdates(this);
//	}
   
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

			gps = new GPS();
			gps.setLatitude(tvLatitude.getText().toString());
			gps.setLongitude(tvLongitude.getText().toString());
			gps.setAltitude(tvAltitude.getText().toString());
			gps.setAccuracy(tvAccuracy.getText().toString());

            return POST(urls[0], gps);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_LONG).show();
       }
    }
	
	
	private boolean validate(){
		if(tvLatitude.getText().toString().trim().equals(""))
			return false;
		else if(tvLongitude.getText().toString().trim().equals(""))
			return false;
		else if(tvAltitude.getText().toString().trim().equals(""))
			return false;
		else if(tvAccuracy.getText().toString().trim().equals(""))
			return false;
		else
			return true;	
	}
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        
        inputStream.close();
        return result;
    }

	@Override
	public void onLocationChanged(Location location) {
		showCurrentLocation(location);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	public void getCurrentLocation(Location location){
		currentLat = location.getLatitude();
		curretLong = location.getLongitude();
		currentAlt = location.getAltitude();
		currentAcc = location.getAccuracy();
	}

	public void showCurrentLocation(Location location){
		getCurrentLocation(location);
		if(currentLat==null){
			locStat.setText("Currently your location is not found");
		}
		tvLatitude.setText(currentLat.toString());
		tvLongitude.setText(curretLong.toString());
		tvAltitude.setText(currentAlt.toString());
		tvAccuracy.setText(currentAcc.toString());
	}
}
