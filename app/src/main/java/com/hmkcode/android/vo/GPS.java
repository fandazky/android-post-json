package com.hmkcode.android.vo;

public class GPS {

	private String latitude;
	private String longitude;
	private String altitude;
	private String accuracy;

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getAltitude() {
		return altitude;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	@Override
	public String toString() {
		return "latitude=" + latitude + ",longitude=" + longitude + ",altitude=" + altitude + ",accuracy=" + accuracy;
	}
	
	
}
